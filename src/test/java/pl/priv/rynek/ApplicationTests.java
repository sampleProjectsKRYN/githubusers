package pl.priv.rynek;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import pl.priv.rynek.client.GitHubRestClient;
import pl.priv.rynek.model.RequestCount;
import pl.priv.rynek.model.RequestCountRepository;
import pl.priv.rynek.model.User;
import pl.priv.rynek.service.ApiService;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;

@SpringBootTest
public class ApplicationTests {

    @Autowired
    RestTemplate restTemplate;
    @Autowired
    private GitHubRestClient mockedGitHubRestClient;
    @Autowired
    private RequestCountRepository mockedRepository;
    @Autowired
    private ApiService mockedApiService;

    private MockRestServiceServer mockRestServiceServer;

    @BeforeEach
    public void setUp() {
        mockRestServiceServer = MockRestServiceServer.createServer(restTemplate);
    }

    @Test
    public void userInformationShouldBeReturnedFromClient() throws URISyntaxException {
        mockRestServiceServer.expect(ExpectedCount.once(),
                        requestTo(new URI("https://api.github.com/users/testUser")))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withStatus(HttpStatus.OK)
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(getSimpleUserResponse()));
        User resultUser = mockedGitHubRestClient.getUserInformation("testUser");
        mockRestServiceServer.verify();
        assertNotNull(resultUser);
        assertAll("resultUser",
                () -> assertEquals(resultUser.getLogin(), "testUser"),
                () -> assertEquals(resultUser.getId(), 583231),
                () -> assertEquals(resultUser.getName(), "Test user"),
                () -> assertEquals(resultUser.getType(), "User")
        );
    }

    @Test
    public void userFollowersCountAndReposCountShouldBeReturnedFromClient() throws URISyntaxException {
        mockRestServiceServer.expect(ExpectedCount.twice(),
                        requestTo(new URI("https://api.github.com/users/testUser")))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withStatus(HttpStatus.OK)
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(getSimpleUserResponse()));
        Long userFollowersCount = mockedGitHubRestClient.getUserFollowersCount("testUser");
        Long userPublicReposCount = mockedGitHubRestClient.getUserPublicReposCount("testUser");
        mockRestServiceServer.verify();
        assertEquals(3910, userFollowersCount);
        assertEquals(8, userPublicReposCount);
    }

    @Test
    public void userWithNoFollowersShouldHaveZeroCalculation() throws URISyntaxException {
        mockRestServiceServer.expect(ExpectedCount.manyTimes(),
                        requestTo(new URI("https://api.github.com/users/testUser2")))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withStatus(HttpStatus.OK)
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(getUserWithNoFollowersResponse()));
        User resultUser = mockedApiService.getUserInformation("testUser2");
        assertNotNull(resultUser);
        assertEquals(0, resultUser.getCalculations());
    }

    @Test
    public void shouldThrowExceptionWhenUserNotFound() throws URISyntaxException {
        mockRestServiceServer.expect(ExpectedCount.manyTimes(),
                        requestTo(new URI("https://api.github.com/users/testUser")))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withStatus(HttpStatus.NOT_FOUND));
        assertThrows(HttpClientErrorException.NotFound.class, () ->
                mockedApiService.getUserInformation("testUser"));
    }

    @Test
    public void testRepositorySaveAndIncrementation() {
        RequestCount testUser = new RequestCount("testUser", 1);
        mockedRepository.save(testUser);
        Optional<RequestCount> userFromRepository = mockedRepository.findById("testUser");
        assertTrue(userFromRepository.isPresent());
        assertEquals(1, userFromRepository.get().getRequestCount());

        mockedApiService.increaseRequestCount("testUser");
        userFromRepository = mockedRepository.findById("testUser");
        assertTrue(userFromRepository.isPresent());
        assertEquals(2, userFromRepository.get().getRequestCount());
    }


    private String getSimpleUserResponse() {
        return "{\n" +
                "  \"login\": \"testUser\",\n" +
                "  \"id\": 583231,\n" +
                "  \"name\": \"Test user\",\n" +
                "  \"type\": \"User\",\n" +
                "  \"avatar_url\": \"https://avatars.githubusercontent.com/u/583231?v=4\",\n" +
                "  \"created_at\": \"2011-01-25T18:44:36Z\",\n" +
                "  \"followers\": 3910,\n" +
                "  \"public_repos\": 8,\n" +
                "  \"followers_url\": \"https://api.github.com/users/octocat/followers\",\n" +
                "  \"repos_url\": \"https://api.github.com/users/octocat/repos\"\n" +
                "}";
    }

    private String getUserWithNoFollowersResponse() {
        return "{\n" +
                "  \"login\": \"testUser2\",\n" +
                "  \"id\": 583232,\n" +
                "  \"name\": \"Test user\",\n" +
                "  \"type\": \"User\",\n" +
                "  \"avatar_url\": \"https://avatars.githubusercontent.com/u/583231?v=4\",\n" +
                "  \"created_at\": \"2011-01-25T18:44:36Z\",\n" +
                "  \"followers\": 0,\n" +
                "  \"public_repos\": 1,\n" +
                "  \"followers_url\": \"https://api.github.com/users/octocat/followers\",\n" +
                "  \"repos_url\": \"https://api.github.com/users/octocat/repos\"\n" +
                "}";
    }
}
