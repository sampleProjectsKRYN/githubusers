package pl.priv.rynek.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import pl.priv.rynek.client.GitHubRestClient;
import pl.priv.rynek.model.RequestCount;
import pl.priv.rynek.model.RequestCountRepository;
import pl.priv.rynek.model.User;

@Slf4j
@Service
public class ApiService {
    private final RequestCountRepository requestCountRepository;
    private final GitHubRestClient gitHubRestClient;

    public ApiService(RequestCountRepository requestCountRepository, GitHubRestClient gitHubRestClient) {
        this.requestCountRepository = requestCountRepository;
        this.gitHubRestClient = gitHubRestClient;
    }

    public User getUserInformation(String login) throws HttpClientErrorException.NotFound, ResourceAccessException {
        User userInformation = gitHubRestClient.getUserInformation(login);
        Long userFollowersCount = gitHubRestClient.getUserFollowersCount(login);
        Long userPublicReposCount = gitHubRestClient.getUserPublicReposCount(login);
        if (userFollowersCount != 0) {
            userInformation.setCalculations(6.0 / userFollowersCount * (2.0 + userPublicReposCount));
        } else {
            log.warn("User with 0 followers, unable to do calculations, setting calculation to 0");
            userInformation.setCalculations(0);
        }
        return userInformation;
    }

    public void increaseRequestCount(String login) {
        requestCountRepository.findById(login)
                .map(requestCount -> {
                    requestCount.setRequestCount(requestCount.getRequestCount() + 1);
                    return requestCountRepository.save(requestCount);
                })
                .orElseGet(() -> {
                    RequestCount newRequestCount = new RequestCount();
                    newRequestCount.setLogin(login);
                    newRequestCount.setRequestCount(1);
                    return requestCountRepository.save(newRequestCount);
                });
    }
}
