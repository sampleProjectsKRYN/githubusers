package pl.priv.rynek.client;

import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import pl.priv.rynek.model.User;

import java.util.HashMap;
import java.util.Map;

@Component
public class GitHubRestClient {
    private static final String GITHUB_USERS_API_URL = "https://api.github.com/users/{login}";
    private final RestTemplate restTemplate;

    public GitHubRestClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public User getUserInformation(String login) {
        UserDto userDto = callGitHubUsersApi(login);
        User userInformation = new User();
        userInformation.setId(userDto.getId());
        userInformation.setLogin(userDto.getLogin());
        userInformation.setName(userDto.getName());
        userInformation.setType(userDto.getType());
        userInformation.setAvatarUrl(userDto.getAvatar_url());
        userInformation.setCreatedAt(userDto.getCreated_at());
        return userInformation;
    }

    public Long getUserFollowersCount(String login) {
        UserDto userDto = callGitHubUsersApi(login);
        return userDto.getFollowers();
    }

    public Long getUserPublicReposCount(String login) {
        UserDto userDto = callGitHubUsersApi(login);
        return userDto.getPublic_repos();
    }

    private UserDto callGitHubUsersApi(String login) {
        Map<String, String> params = new HashMap<>();
        params.put("login", login);
        return restTemplate.getForObject(GITHUB_USERS_API_URL, UserDto.class, params);
    }

}
