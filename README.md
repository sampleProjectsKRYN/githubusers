# GitHubUsers
Serwis RESTowy który zwraca informacje pobrane z API wystawionego przez GitHub.
Api serwisu zwraca informacje o użytkowniku w następującym formacie:
```` JSON
GET http://localhost:8080/GitHubUsers/users/octocat
{
  "id": 583231,
  "login": "octocat",
  "name": "The Octocat",
  "type": "User",
  "avatarUrl": "https://avatars.githubusercontent.com/u/583231?v=4",
  "createdAt": "2011-01-25T18:44:36",
  "calculations": 0.01534134492457172
}
````

Serwis pobiera dane o używkowniku z API GitHub ( https://api.github.com/users/octocat). 
Pole calculations zwracany jest wynik działania 6 / liczba_followers * (2 + liczba_public_repos).

Aplikacja zapisuje w bazie danych (aktualnie skonfigurowana na bazę H2 w pamięci) ilość wywołań dla każdego użytkownika.
